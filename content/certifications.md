+++
title = "Certifications"
date = "2020-07-19"
aliases = ["certifications"]
[ author ]
  name = "Stuart Lupton"
+++

| Date Obtained &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Certification |
|---|---|
| &nbsp; | &nbsp; |
| June 2020 | AZ300 - Microsoft Azure Architect Technologies	 |
| May 2020 | AZ103 - Microsoft Azure Administrator |
| &nbsp; | &nbsp; |
| October 2018 | AWS Certified Solutions Architect - Professional |
| &nbsp; | &nbsp; |
| September 2017 | AWS Certified SysOps Administrator - Associate |
| August 2017 | AWS Certified Developer - Associate |
| July 2017 | AWS Certified Solutions Architect - Associate |
| Janurary 2017 | Prince2 Foundation |
| &nbsp; | &nbsp; |
| Feburary 2015 | 70-663 - Designing and Deploying Exchange Server 2010 |
| Jan 2015 | 70-622 Microsoft Exchange Server 2010, Configuring	 |
| &nbsp; | &nbsp; |
| April 2014 | 70-411 - Administering Windows Server 2012	 |
| March 2014 | 70-410 - Installing and Configuring Windows Server 2012	 |
| &nbsp; | &nbsp; |
| October 2008 | MCSE 2003 (Exam's 270, 290, 291, 299 & 298) |
