+++
title = "About Me"
date = "2020-07-19"
aliases = ["about-me"]
[ author ]
  name = "Stuart Lupton"
+++

I work as a **Cloud Solutions Architect** helping customers bridge the gap between vision and technical implementation.

My commercial experience spans across various sectors including Defence & Security, Public, Insurance and Transportation/Logistics.

Coming from a infrastructure background and having experience in devops, technical architecture design, cloud security and business continuity places me in a good position to resolve business problems with cloud solutions.

I am based in Doncaster, UK but I often travel throughout Europe helping customers address business problems and accelerate the adoption of cloud services.

In my spare time when I am not spending time with my wife and two children or fishing, I love to build and develop my skills further in areas that I would not usually get the opportunity to be involved in. Keep an eye out for these projects in my [Blog](http://stuartlupton.com/blog).